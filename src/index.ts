import * as Alexa from 'alexa-sdk';
import AWS = require('aws-sdk');

import { Config } from './config';

const dynamoDB = new AWS.DynamoDB.DocumentClient();

export function handler(event, context, callback) {
    const alexa = Alexa.handler(event, context, callback);
    alexa.appId = Config.appId;

    alexa.registerHandlers(handlers);

    alexa.execute();
}

const handlers = {
    'LaunchRequest': function () {

        // If they give us no information, we need to kick off the questions
        this.response.speak("Tell me a Trax station and I'll let you know when the next train is").listen('Which station?');
        this.emit(':responseReady');

    },
    'getnexttrain': async function () {

        // If they haven't given us the station or direction yet, we need to ask them for it
        if (this.event.request.dialogState === "STARTED") {
            return this.emit(':delegate', this.event.request.intent);
        }

        const utils = new Utils();

        const slotValues = utils.getSlotValues(this.event.request.intent.slots) as any;

        try {
            const stations = await utils.getStop(slotValues.Station.resolved);

            if (stations.Items.length < 1) {
                return this.emit(":tell", "That station does not exist");
            }

            // This should come back with a single item
            const station = stations.Items[0];
            let lineColor = slotValues.LineColor.resolved;

            if (station.lines) {
                const keys = Object.keys(station.lines);

                // If there is more than one line, we need to ask which line the user wants
                // If they told us a wrong color, ask again
                if (keys.length > 1 && (!lineColor || !station.lines[lineColor])) {

                    const str = utils.delimitString(keys, ', ', ' or ');

                    return this.emit(':elicitSlot', 'LineColor', 'Which line: ' + str + '?');
                } else if (keys.length === 1) {
                    // If there is only one line in the list we just choose that
                    lineColor = keys[0];
                }
            }

            const details = utils.getDetails(station, slotValues.Direction.resolved, lineColor, slotValues.Time.resolved);

            this.emit(':tell', `The next ${lineColor} line train at ${station.stationName} ${details.direction} is at ${details.time}.`);
        } catch (err) {
            console.log(err);
            this.emit(':tell', 'There was an error');
        }

    },
    'SessionEndedRequest': function () {
        this.emit(':tell', 'Goodbye');
    }
}



class Utils {
    getStop(station) {
        const params = {
            TableName: 'stations',
            KeyConditionExpression: '#name = :station',
            ExpressionAttributeNames: {
                "#name": "stationName"
            },
            ExpressionAttributeValues: {
                ":station": station
            }
        }

        return dynamoDB.query(params).promise();
    }

    getSlotValues(filledSlots) {
        //given event.request.intent.slots, a slots values object so you have
        //what synonym the person said - .synonym
        //what that resolved to - .resolved
        //and if it's a word that is in your slot values - .isValidated
        let slotValues = {};

        Object.keys(filledSlots).forEach(function (item) {
            var name = filledSlots[item].name;
            if (filledSlots[item] &&
                filledSlots[item].resolutions &&
                filledSlots[item].resolutions.resolutionsPerAuthority[0] &&
                filledSlots[item].resolutions.resolutionsPerAuthority[0].status &&
                filledSlots[item].resolutions.resolutionsPerAuthority[0].status.code) {

                switch (filledSlots[item].resolutions.resolutionsPerAuthority[0].status.code) {
                    case "ER_SUCCESS_MATCH":
                        slotValues[name] = {
                            "synonym": filledSlots[item].value,
                            "resolved": filledSlots[item].resolutions.resolutionsPerAuthority[0].values[0].value.name,
                            "isValidated": filledSlots[item].value == filledSlots[item].resolutions.resolutionsPerAuthority[0].values[0].value.name
                        };
                        break;
                    case "ER_SUCCESS_NO_MATCH":
                        slotValues[name] = {
                            "synonym": filledSlots[item].value,
                            "resolved": filledSlots[item].value,
                            "isValidated": false
                        };
                        break;
                }
            } else {
                slotValues[name] = {
                    "synonym": filledSlots[item].value,
                    "resolved": filledSlots[item].value,
                    "isValidated": false
                };
            }
        });
        return slotValues;
    }

    getDetails(station, direction, lineColor, time?) {
        const now = new Date();
        const day = (now.getDay() >= 6) ? 'weekend' : 'weekday';
        const dirStr = this.getDestination(lineColor, direction);

        let hour;
        let minute;

        if (time) {
            let d = this.parseTime(time);
            hour = d.hour;
            minute = d.minute;
        }
        else {
            let h = now.getHours();

            hour = (h + (h < 6 ? 24 : 0)) - 6;
            minute = now.getMinutes();
        }

        const times = station.lines[lineColor][direction][day];
        let next = times[0];

        for (let i = 0; i < times.length; i++) {
            const time = this.parseTime(times[i]);

            if (hour > time.hour || (hour == time.hour && minute > time.minute)) {
                continue;
            }

            next = times[i];
            break;
        }

        return { time: next, direction: dirStr };
    }

    parseTime(str) {
        const s = str.split(/(:|[AaPp][Mm])/g);
        let hour = +s[0];
        const minute = +s[2];
        const meridian = s[3];

        // convert to 24 hour time
        if (meridian === 'PM' && hour < 12) {
            hour += 12;
        }else if(meridian === 'AM' && hour === 12){
            hour = 0;
        }

        return { hour: hour, minute: minute };
    }

    getDestination(line, direction) {
        const lines = {
            'rednorth': 'to Medical',
            'redsouth': 'to Daybreak',
            'bluenorth': 'to Salt Lake Center',
            'bluesouth': 'to Draper',
            'greennorth': 'to Airport',
            'greensouth': 'to West Valley'
        }

        return lines[line + '' + direction];
    }

    delimitString(keys: Array<string>, delimiter: string, last: string): string{
        return keys.reduce((acc, item, i, arr) => {
            return acc + (i == arr.length - 1 ? last : delimiter) + item;
        });
    }
}
